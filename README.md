#3dmodels

##models for 3D prints. Created in FreeCAD.

- ardo-micro-box - box for arduino micro/nano PCB
- creeper - minecraft creeper
- ftdi-ttl-box - box for fdti rs232 ttl PCB
- parrot-mike-holder - mikrophone holder for parrot - car handsfree 
- pebble-overlay - cover for your pebble
- ramps-box - box for the RAMPS+ Arduino MEGA PCB on REBEL2
- shop cart coin - coin for shopping card
- stojan na noty - parts of note stand
- wrench-label - stylish door name label

